package com.lindaexchange.lindaexchangeadmin;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RateValueUpdateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RateValueUpdateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RateValueUpdateFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;
    private int branchIndex;
    private RecyclerView recyclerView;
    private BuySellRecyclerViewAdapter adapter;
    private ProgressBar mProgressView;
    private FloatingActionButton fab;
    private List<RateValueDB> rateValueList;

    private CountDownTimer timer = new CountDownTimer(10000, 10000) {
        @Override
        public void onTick(long millisUntilFinished) { }

        @Override
        public void onFinish() { showTimeoutSnackbar(); }
    };

    private OnFragmentInteractionListener mListener;

    public RateValueUpdateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RateValueUpdateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RateValueUpdateFragment newInstance(String param1, String param2) {
        RateValueUpdateFragment fragment = new RateValueUpdateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            branchIndex = getArguments().getInt("branchIndex", 0);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_rate_value_update, container, false);
        recyclerView = view.findViewById(R.id.list);
        mProgressView = view.findViewById(R.id.progressBar);
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRateFromUser();
            }
        });

        showProgress(true);
        String rateString = getString(R.string.rate);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(rateString);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                GenericTypeIndicator<List<ExchangeRateDB>> t = new GenericTypeIndicator<List<ExchangeRateDB>>() {};
//                List<ExchangeRateDB> rateDBList = dataSnapshot.getValue(t);
                rateValueList = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    ExchangeRateDB rateDB = child.getValue(ExchangeRateDB.class);
                    rateDB.setKey(child.getKey());
                    rateValueList.addAll(rateDB.extractRateValue(branchIndex));
                }
//                for (ExchangeRateDB rate : rateDBList) {
//                    rateValueList.addAll(rate.extractRateValue(branchIndex));
//                }
                adapter = new BuySellRecyclerViewAdapter(rateValueList, branchIndex, mListener);
                recyclerView.setAdapter(adapter);
                showProgress(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showProgress(false);
                showAlert("ERROR!");
            }
        });
        return view;
    }

    private void saveRateFromUser() {
        Map<String, Object> map = adapter.getMap();
        saveRate(map);
    }

    private void saveRate(Map<String, Object> map) {
        showProgress(true);
        Log.d("123", "saveRate: " + map.toString());
        String rateString = getString(R.string.rate);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(rateString);
        ref.updateChildren(map, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    saveTimestamp();
                } else {
                    showAlert("ERROR!");
                    showProgress(false);
                }
            }
        });
    }

    private void saveTimestamp() {
        Map<String, Object> map = new HashMap<>();
        String updateTimeString = getString(R.string.update_time);
        String timestampString = getString(R.string.timestamp);

        map.put(updateTimeString + "/0/" + timestampString, System.currentTimeMillis());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        ref.updateChildren(map, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getString(R.string.finish))
                            .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getFragmentManager().popBackStack();
                                }
                            }).show();
                } else {
                    showAlert("ERROR!");
                }
                showProgress(false);
            }
        });
    }

    private void showAlert(String title) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
            builder.setTitle(title)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue
                        }
                    })
                    .show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.rate_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_load:
                checkRateFromWebsite();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(final boolean show) {
        if (show) {
            timer.start();
        } else {
            timer.cancel();
        }

        int shortAnimTime = 200;
        try {
            shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        recyclerView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        fab.setVisibility(show ? View.GONE : View.VISIBLE);
        fab.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                fab.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void showTimeoutSnackbar() {
        Snackbar snackbar = Snackbar.make(recyclerView, R.string.timeout_string, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    private void checkRateFromWebsite() {
        new CheckRate().execute();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
//        void onFragmentInteraction(Uri uri);
    }

    private class CheckRate extends AsyncTask<Void, Void, String> {
        private Map<String, Object> updateMap;

        @Override
        protected void onPreExecute() {
            showProgress(true);
            updateMap = new HashMap<>();
        }

        @Override
        protected String doInBackground(Void... voids) {
            // Create a String for result to show on the AlertDialog
            String result = "";
            try  {
                Document doc = null;
                try {
                    URL url = new URL("http://www.lindaexchange.com/EN/exchange");
                    doc = Jsoup.parse(url, 3000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Elements elements = doc.select("table");
                Elements elementbody = elements.get(0).select("tbody");
                Elements rows = elementbody.get(0).select("tr");

                for (Element row : rows) {

                    if (row.select("td").size() == 4) {
                        String country = row.select("td").get(0).text();
                        String currency = row.select("td").get(1).text();
                        String buy = row.select("td").get(2).text();
                        String sell = row.select("td").get(3).text();
                        Log.d("row",country+currency+buy+":"+sell);

                        for (RateValueDB val : rateValueList) {
                            String currencyName = val.getCurrencyname() + val.getDenominationname();
                            String appBuy = val.getBuy();
                            String appSell = val.getSell();
                            currency = currency.replace(" ", "");

                            if (currencyName.equals(currency)) {
                                // Found currency

                                if (!appBuy.equals(buy)) {
//                                    Log.d("row", appBuy + " -> " + buy);
                                    result += currencyName + " Buy" + " : " + appBuy + " -> " + buy + "\n";
                                    updateMap.putAll(val.getMapBuy(buy));
                                    Log.d("row", val.getMapBuy(buy).toString());
                                }

                                if (!appSell.equals(sell)) {
//                                    Log.d("row", appSell + " -> " + sell);
                                    result += currencyName + " Sell" + " : " + appSell + " -> " + sell + "\n";
                                    updateMap.putAll(val.getMapSell(sell));
                                    Log.d("row", val.getMapSell(sell).toString());
                                }

                                Log.d("row", currencyName + " = " + currency);
                            } else {
                                Log.d("notMatch", currencyName + " = " + currency);
                            }
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            showProgress(false);

            // Show No Change message if string == ""
            if (s.equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.rate_check_no_change))
                        .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(s)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                saveRate(updateMap);

                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
            }
        }
    }
}
